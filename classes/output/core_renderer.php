<?php
// This file is part of the classic theme for Moodle
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace theme_campwks\output;
use moodle_url;
use stdClass;
use pix_icon;
use core_text;
use action_menu;
use html_writer;
use custom_menu;
use context_course;

defined('MOODLE_INTERNAL') || die;


/**
 * Renderers to align Moodle's HTML with that expected by Bootstrap
 *
 *
 * @package    theme_campwks
 * @copyright  2022 Amit Singh (web.amitsingh@gmail.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class core_renderer extends \theme_boost\output\core_renderer {

    /**
     * @wkstheme override: change breadcrumbs
     * @return string
     */
    public function navbar(): string {
        global $DB;
        if ($this->page->context->contextlevel == CONTEXT_COURSE) {
            $category = $DB->get_record('course_categories', array('id' => $this->page->course->category));
            if($category !== false) {
                $url = new moodle_url("/course/index.php?categoryid={$category->id}");
                return "<a href='{$url}'>{$category->name}</a>";
            } else {
                return "";
            }

        }

        if ($this->page->context->contextlevel == CONTEXT_MODULE) {

            $url = "/course/view.php?id={$this->page->course->id}";
            if ($this->page->cm !== null) {
                 $url .= "#coursecontentcollapse" . "{$this->page->cm->sectionnum}";
            }

            $url = new moodle_url($url);

            //$linkLabel = get_string('back_to_course', 'theme_campwks');
            $linkLabel = $this->page->course->fullname;
            $maxChars = 60;
            if (strlen($linkLabel) > $maxChars) {
                $linkLabel = substr($linkLabel, 0, $maxChars - 3) . ' ..';
            }

            return '
                <div style="display:flex;flex-direction: row; padding-bottom:1.6rem; vertial-align:middle; height:24px">
                    <a href="'.$url.'">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" stroke-width="1.5" stroke="currentColor" viewBox="0 0 24 24" style="width:16px;display:inline-block; padding-bottom:4px">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M10.5 19.5L3 12m0 0l7.5-7.5M3 12h18"></path>
                    </svg><span style="margin-left:8px;display:inline-block">'.$linkLabel.'</span> </a>    
                </div>               
            ';
        }
        // default: do not show any breadcrumbs
        return "";
    }

    /**
     * Renders the login form.
     *
     * @param \core_auth\output\login $form The renderable.
     * @return string
     */
    public function render_login(\core_auth\output\login $form) {
        global $CFG, $SITE;
        global $OUTPUT;
        $context = $form->export_for_template($this);

        $context->errorformatted = $this->error_text($context->error);
        $context->companyinfo = theme_campwks_get_setting('companyinfo');
        $context->loginpageimg = get_loginimage_url();
        $url = get_loginlogo_url();
        $context->logourl = $url;
        $context->sitename = format_string($SITE->fullname, true,
                ['context' => context_course::instance(SITEID), "escape" => false]);

        $context->is_wks = false;
        $context->is_kvlu = false;
        $context->is_kvbsz = false;

        $context->wks_logo =  $OUTPUT->image_url('wks_logo', 'theme');
        $context->wst_logo =  $OUTPUT->image_url('wst_logo', 'theme');

        $theme = \theme_config::load('campwks');
        if ($theme->settings->preset === \TenantEnum::WKS) {
            $context->is_wks = true;
        } elseif ($theme->settings->preset === \TenantEnum::KVLU) {
            $context->is_kvlu = true;
        } elseif ($theme->settings->preset === \TenantEnum::KVBSZ) {
            $context->is_kvbsz = true;
        }

        return $this->render_from_template('theme_campwks/core/loginform', $context);
    }

    /**
     * The standard tags that should be included in the <head> tag
     * including a meta description for the front page
     *
     * @return string HTML fragment.
     */
    public function standard_head_html() {
        global $CFG;
        require_once "$CFG->dirroot/theme/campwks/Enums/TenantEnum.php";

        $output = parent::standard_head_html();

        // Allow custom head content while in development.
        if (debugging('', DEBUG_DEVELOPER) && !empty($CFG->devel_custom_additional_head)) {
            $output .= $CFG->devel_custom_additional_head;
        }

        // Font: tenant specific
        $output .= '<link rel="preconnect" href="https://fonts.googleapis.com">';
        $output .= '<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>';

        $theme = \theme_config::load('campwks');
        if ($theme->settings->preset === \TenantEnum::WKS) {
            $output .= '<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap" rel="stylesheet">';
        } elseif ($theme->settings->preset === \TenantEnum::KVLU) {
            $output .= '<link href="https://fonts.googleapis.com/css?family=Bree+Serif&amp;display=swap" rel="stylesheet">';
        } elseif ($theme->settings->preset === \TenantEnum::KVBSZ) {
            $output .= '<link href="https://fonts.googleapis.com/css2?family=Crimson+Text:wght@400;700&display=swap" rel="stylesheet">';
        }

        // Without extensions; Moodle finds out by itself.
//        $icons = [
//            'apple-touch-icon',
//            'favicon-16x16',
//            'favicon-32x32',
//            'safari-pinned-tab',
//        ];
//
//        $data = new \stdClass();
//        $data->iconurls = new \stdClass();
//        foreach ($icons as $icon) {
//            $data->iconurls->{$icon} = $this->image_url('favicons/' . $icon, 'theme');
//        }
//
//        // Add favicon config for all platforms.
//        $output .= self::render_from_template('theme_wksboost/head_favicons', $data);

        // Add SCSS generation fixes (See https://liip.slack.com/messages/C2ZU98G31/convo/C0D09PLNB-1561981660.000900/).
        // See theme/wksboost/scss/components/block_wks_dashboard.scss:119 => calc((100% - 250px) / 8).
//        $url = new moodle_url("/theme/wksboost/fix.css");
//        $output .= sprintf('<link rel="stylesheet" type="text/css" href="%s" />', $url->out_as_local_url(false));

        return $output;
    }

    public function get_wks_logo_url(): string
    {
        global $OUTPUT;
        return $OUTPUT->image_url('bino', 'theme');
    }
}
